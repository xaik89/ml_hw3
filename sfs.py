def sfs(x, y, k, clf, score):
    """
    :param x: feature set to be trained using clf. list of lists.
    :param y: labels corresponding to x. list.
    :param k: number of features to select. int
    :param clf: classifier to be trained on the feature subset.
    :param score: utility function for the algorithm, that receives clf, feature subset and labeles, returns a score. 
    :return: list of chosen feature indexes
    """

    def feature_subset(features_idxs):
        return list(map(lambda l: [l[i] for i in features_idxs], x))

    output_features = list()
    all_features = list(range(len(x[0])))

    while len(output_features) != k:
        max_U = 0
        best_feature = None
        for feature_idx in all_features:
            curr_U = score(clf, feature_subset(output_features + [feature_idx]), y)
            if curr_U > max_U:
                max_U = curr_U
                best_feature = feature_idx

        output_features.append(best_feature)
        all_features.remove(best_feature)

    return output_features

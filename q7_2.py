import pandas as pd
from sklearn.model_selection import train_test_split, cross_val_predict
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
import sfs

input_file = "flare.csv"
df = pd.read_csv(input_file)

# provided your csv has header row, and the label column is named "classification"
target = df["classification"]
# select all but the last column as data
data = df.ix[:, :-1]

X_train, X_test, y_train, y_test = train_test_split(data,
                                                    target,
                                                    test_size=0.25,
                                                    random_state=4)
# seif a
clf = KNeighborsClassifier(5)

clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

print("accuracy without sfs =", accuracy_score(y_pred.astype(int), y_test.astype(int)))

# seif b
clf = KNeighborsClassifier(5)


def score(clf, sub_data, y):
    import copy
    changed_data = copy.deepcopy(sub_data)
    del changed_data[0]

    df = pd.DataFrame(changed_data)
    df.columns = sub_data[0]
    df2 = pd.DataFrame(y)

    y_pred = cross_val_predict(clf, df, df2.values.ravel(), cv=4)
    return accuracy_score(y_pred.astype(int), df2.astype(int))


X = [X_train.columns.tolist()] + X_train.values.tolist()
Y = y_train.tolist()

property_idxs = sfs.sfs(X, Y, 8, clf, score)
X_train_improved = X_train.iloc[:, property_idxs]
X_test_improved = X_test.iloc[:, property_idxs]
clf.fit(X_train_improved, y_train)
y_pred = clf.predict(X_test_improved)

print("accuracy with sfs =", accuracy_score(y_pred.astype(int), y_test.astype(int)))




import pandas as pd
from sklearn.model_selection import train_test_split, cross_val_predict
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier

input_file = "flare.csv"
df = pd.read_csv(input_file)

# provided your csv has header row, and the label column is named "classification"
target = df["classification"]
# select all but the last column as data
data = df.ix[:, :-1]

X_train, X_test, y_train, y_test = train_test_split(data,
                                                    target,
                                                    test_size=0.25,
                                                    random_state=3)

clf1 = DecisionTreeClassifier(criterion="entropy")

clf2 = DecisionTreeClassifier(criterion="entropy", min_samples_split=20)

#seif c
print("seif c:")
clf1.fit(X_train, y_train)
clf2.fit(X_train, y_train)

pred1 = clf1.predict(X_test)
print(accuracy_score(pred1.astype(int), y_test.astype(int)))
pred2 = clf2.predict(X_test)
print(accuracy_score(pred2.astype(int), y_test.astype(int)))
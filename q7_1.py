import pandas as pd
from sklearn.model_selection import train_test_split, cross_val_predict
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
import sfs

input_file = "flare.csv"
df = pd.read_csv(input_file)

# provided your csv has header row, and the label column is named "classification"
target = df["classification"]
# select all but the last column as data
data = df.ix[:, :-1]

X_train, X_test, y_train, y_test = train_test_split(data,
                                                    target,
                                                    test_size=0.25,
                                                    random_state=4)

clf = KNeighborsClassifier()

clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

print(accuracy_score(y_pred.astype(int), y_test.astype(int)))




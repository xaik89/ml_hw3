import pandas as pd
from sklearn.model_selection import KFold
from sklearn.metrics import confusion_matrix, accuracy_score
from id3 import Id3Estimator
import numpy as np

input_file = "flare.csv"
df = pd.read_csv(input_file)

# provided your csv has header row, and the label column is named "classification"
target = df["classification"]
# select all but the last column as data
data = df.ix[:, :-1]

# prune by default False
clf = Id3Estimator()
kf = KFold(n_splits=4)
kf_sum = 0
pred_global = list()
for traincv, testcv in kf.split(data):
    clf.fit(np.array(data)[traincv], np.array(target)[traincv])
    pred = clf.predict(np.array(data)[testcv])
    pred_global += list(pred.astype(int))
    kf_sum += accuracy_score(pred.astype(int), np.array(target)[testcv].astype(int))

average_acc = kf_sum/4

conf_mat = confusion_matrix(target.astype(int), pred_global)

print(average_acc)
print(conf_mat)

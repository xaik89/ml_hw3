import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier

input_file = "flare.csv"
df = pd.read_csv(input_file)

# provided your csv has header row, and the label column is named "classification"
target = df["classification"]
# select all but the last column as data
data = df.ix[:, :-1]

X_train, X_test, y_train, y_test = train_test_split(data,
                                                    target,
                                                    test_size=0.25,
                                                    random_state=0)

# by defining criterion="entropy" sklearn use id3 tree.
clf1 = DecisionTreeClassifier(criterion="entropy", min_samples_split=2)
clf2 = DecisionTreeClassifier(criterion="entropy", min_samples_split=200)

# over fitting
clf1.fit(X_train, y_train)

# under fitting
clf2.fit(X_train, y_train)

y_pred1 = clf1.predict(X_test)
y_pred2 = clf2.predict(X_test)

y_pred3 = clf1.predict(X_train)
y_pred4 = clf2.predict(X_train)

accuracy1 = accuracy_score(y_pred1.astype(int), y_test.astype(int))
accuracy2 = accuracy_score(y_pred2.astype(int), y_test.astype(int))

accuracy3 = accuracy_score(y_pred3.astype(int), y_train.astype(int))
accuracy4 = accuracy_score(y_pred4.astype(int), y_train.astype(int))

print("accuracy for over fitting for test set: ", accuracy1, " and for train set: ", accuracy3)
print("accuracy for under fitting for test set: ", accuracy2, " and for train set: ", accuracy4)

import pandas as pd
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
import sfs

input_file = "flare.csv"
df = pd.read_csv(input_file)

# provided your csv has header row, and the label column is named "classification"
target = df["classification"]
# select all but the last column as data
data = df.ix[:, :-1]

clf1 = KNeighborsClassifier(5)


def score(clf, sub_data, y):
    import copy
    changed_data = copy.deepcopy(sub_data)
    del changed_data[0]

    df = pd.DataFrame(changed_data)
    df.columns = sub_data[0]
    df2 = pd.DataFrame(y)

    y_pred = cross_val_predict(clf, df, df2.values.ravel(), cv=4)
    return accuracy_score(y_pred.astype(int), df2.astype(int))


X = [data.columns.tolist()] + data.values.tolist()
Y = target.tolist()

property_idxs = sfs.sfs(X, Y, 8, clf1, score)
X_data_improved = data.iloc[:, property_idxs]
X_test_improved = data.iloc[:, property_idxs]
scores1 = cross_val_score(clf1, X_data_improved, target, cv=4)

print("accuracy of q7 = ", sum(scores1)/4)

clf2 = DecisionTreeClassifier(criterion="entropy", min_samples_split=20)

scores2 = cross_val_score(clf2, data, target, cv=4)
print("accuracy of q8 = ", sum(scores2)/4)

import pandas as pd
from sklearn.model_selection import cross_val_predict, train_test_split
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
import sfs

input_file = "flare.csv"
df = pd.read_csv(input_file)

# provided your csv has header row, and the label column is named "classification"
target = df["classification"]
# select all but the last column as data
data = df.ix[:, :-1]

X_train, X_test, y_train, y_test = train_test_split(data,
                                                    target,
                                                    test_size=0.25,
                                                    random_state=4)

# q7
clf1 = KNeighborsClassifier(5)


def score(clf, sub_data, y):
    import copy
    changed_data = copy.deepcopy(sub_data)
    del changed_data[0]

    df = pd.DataFrame(changed_data)
    df.columns = sub_data[0]
    df2 = pd.DataFrame(y)

    y_pred = cross_val_predict(clf, df, df2.values.ravel(), cv=4)
    return accuracy_score(y_pred.astype(int), df2.astype(int))


X = [X_train.columns.tolist()] + X_train.values.tolist()
Y = y_train.tolist()

property_idxs = sfs.sfs(X, Y, 8, clf1, score)
X_train_improved = X_train.iloc[:, property_idxs]
X_test_improved = X_test.iloc[:, property_idxs]
clf1.fit(X_train_improved, y_train)
y_pred1 = clf1.predict(X_test_improved)

score1 = accuracy_score(y_pred1.astype(int), y_test.astype(int))

clf3 = KNeighborsClassifier(5)
clf3.fit(X_train, y_train)
pred3 = clf3.predict(X_test)
score3 = accuracy_score(pred3.astype(int), y_test.astype(int))

print("accuracy of KNN with wrapper method =", score1, "without:", score3)

# q8
clf2 = DecisionTreeClassifier(criterion="entropy", min_samples_split=20)
clf4 = DecisionTreeClassifier(criterion="entropy")

clf2.fit(X_train, y_train)
clf4.fit(X_train, y_train)

pred1 = clf2.predict(X_test)
score1 = accuracy_score(pred1.astype(int), y_test.astype(int))
pred2 = clf4.predict(X_test)
score2 = accuracy_score(pred2.astype(int), y_test.astype(int))

print("accuracy of id3 with embedded method =", score1, "without:", score2)

